const express = require("express");
const app = express();
const cors = require("cors");
app.use(cors());

// api routes
app.get('/', (req, res) => {
	console.log(req);
	res.send(`Hello World! I am ${process.env.ENV} on port ${process.env.PORT}`)
  })

const port = process.env.PORT || 2000;

const server = app.listen(port, "0.0.0.0", function () {
    console.log("Server listening on port " + port);
});
